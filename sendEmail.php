<?php
header('Access-Control-Allow-Origin: *');

if (isset($_POST['email'])) {
    
    $email = htmlspecialchars($_POST['email']);
    $clientId = 'gendd6kvhmfptvsn';
    $apiKey = 'j2q8qp4fbp9qf2b8p49fb';
    $apiSecret = 'f999640d68854291a7115a0c2f0c7b08';
    $user = 'marketing@salesmanago.pl';            //KONTO UŻYTKONIKA W SYSTEMIE SALESMANAGO
    $emailId = 'c16cdaec-8004-475e-9747-198337484ebe';
    $sha1 = sha1($apiKey . $clientId . $apiSecret);

    function do_post_request($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );

        return curl_exec($ch);
    }

    $dt = new DateTime('NOW');

    $data = array(
        'clientId' => $clientId,
        'apiKey' => $apiKey,
        'requestTime' => time(),
        'sha' =>  $sha1,
        'emailId' => $emailId,
        'user' => $user,
        'date' => $dt->format('c'),                //DATA WYSŁANIA 
        'contacts' => array(array(
            "addresseeType" => "EMAIL",
            'value' => $email
        )),
    );

    $result = do_post_request('http://www.salesmanago.pl/api/email/sendEmail', json_encode($data));   //INSTANCJA APLIKACJI, gdy posiadacie Państwo konto na app2.salesmanago.pl proszę zmienić

    $r = json_decode($result);
    echo $r->conversationId;
}
?>
<?php

function modify_jquery() {
    if (!is_admin()) {
        // comment out the next two lines to load the local copy of jQuery
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4');
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'modify_jquery');

function workshops_salesmanago_scripts() {

wp_enqueue_style( 'vendors', get_template_directory_uri() . '/assets/css/vendors.min.css', array(), null, 'all');
wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array(), null, 'all');
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), null, 'all' );


    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4');
    wp_enqueue_script('jquery');

    wp_enqueue_script( 'scrollTo', get_template_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'), null);


    /*
    wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
wp_enqueue_script( 'vendors', get_template_directory_uri() . '/assets/js/vendors.min.js', array('jquery'), null, 'all' );
wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.min.js', array('jquery'), null, 'all' );
    */
/*

wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css');


wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.1.1.min.js', array('jquery'), null, true );
wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), null, true );
wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), null, true );

wp_enqueue_script( 'fitagain-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

wp_enqueue_script( 'fitagain-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
*/

}
add_action( 'wp_enqueue_scripts', 'workshops_salesmanago_scripts' );
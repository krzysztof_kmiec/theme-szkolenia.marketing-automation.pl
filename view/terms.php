<?php
/**
 * Template Name: terms
 **/
?>

<?php get_header(); ?>

<div class="slider" style="min-height: auto;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-name">
                    <h1 class="top-name" style="padding: .5em;">Terms &amp; Conditions</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="terms">
	<div class="container">
		<p class="terms__heading"><strong>I General</strong></p>
		<ol class="terms__list">
			<li>By registering for the Marketing automation Workshops, you hereby agree to these Terms and Conditions, which form a legal contract between you, the Attendee, and Marketing automation Workshops&#39;s Partners, whose Organizer is Benhauer Sp. z o. o., address: Grzegórzecka 21, 31-532 Kraków, a company which has been registered to The Business Registry of the District Court for Kraków-Śródmieście in Kraków, XII Division of the National Court Register (KRS), KRS number 0000523346.</li>
			<li>The Organizer reserves the right to change any aspects of the Marketing automation Workshops schedule due to reasons beyond the Organiser&#39;s control, caused by an unforeseen negative event or transport difficulties and has an obligation to post up-to- date information on the website.</li>
			<li>Organizer provides complimentary tea and coffee, as well as a sandwich buffet, for Attendees and Partners during the Marketing automation Workshops.</li>
			<li>Terms and definitions used in these Terms &amp; Conditions have the following meaning:</li>
		</ol>
		<p><strong>Organizer</strong>: Benhauer Sp. z o. o., address: Grzegórzecka 21, 31-532 Kraków, a company which has been registered to The Business Registry of the District Court for Kraków-Śródmieście in Kraków, XII Division of the National Court Register (KRS), KRS number 0000523346.</p>
		<p><strong>Attendee</strong>: a natural person, a legal person or an organizational entity without legal personality which doesn&#39;t act as a consumer in the meaning  of legal definition pointed in article 221 of Polish Civil Code, registering using the Registration Form available online at the Marketing automation Workshops website.</p>
		<p><strong>Partner</strong>: a third party, a natural or legal person who has purchased advertising services according to Partner Offers cost.</p>
		<p><strong>Attendance confirmation</strong>: confirmation sent via email by Organizator. Marketing automation Workshops: workshops during which Attendee is trained to gain unique Marketing Automation knowledge , later on called „the Marketing automation Workshops”.</p>
		<br>
		<br>
		<strong>II Confirmation of participation</strong>
		<ol class="terms__list">
			<li>In accordance with these Regulations, filling out the form is tantamount to concluding an agreement with the Organizer. That means - by choosing the right option in this form - agreement on the Regulations, and adherence to the rules and other arrangements made between Attendee and Organizator. Also, that applies to participation payment obligation.</li>
			<li>Participants have to register at the latest 3 days prior to the Marketing automation Workshops.</li>
			<li>An Attendee shall be obligated to fill in the registration form accurately.</li>
			<li>Organizator will not be held liable for losses caused by providing incorrect Participant data in the registration form.</li>
			<li>Organizator reserves the right to have Attendees list at their disposal and publish names of the companies along with the Attendees’ job titles.</li>
			<li>Filling out the registration form is tantamount to concluding an agreement on sharing Attendee’s given e-mail address to the third parties cooperating with Organizer as part of the specific Conference and receiving marketing messages by email such as Newsletter from the Organizer or other channels. In particular, on the processing of data by Organizer and third parties, who Organizer cooperate with during this Marketing automation Workshops or another Conference. An Attendee, who do not agree, is obligated to inform an Organizer about a decision at workshops@salesmanago.pl, right after completing the registration form.</li>
			<li>Every registered Attendee receives a badge and the Marketing automation Workshops materials.</li>
			<li>The Marketing automation Workshops materials can be extended with additional material from the Marketing automation Workshops Partners.</li>
		</ol>
		<p class="terms__heading"><strong>III Payment</strong></p>
		<ol class="terms__list">
			<li>Registered Attendee is obligated to pay the  attendance fee for the ticket.</li>
			<li>The cost of attendance determined by Organizer is 199 € gross per one ticket.</li>
			<li>In case of buying more tickets, the Organizer is entitled to grant the Attendee a discount. The discount will be awarded by sending to the e-mail address of the Attendee information about the award, the number of tickets for which it was granted and the amount of the discount.</li>
			<li>Attendee makes a payment online or by credit card or by using PayPal service. The Payment should be made within 3 days until completing the registration form.</li>
			<li>Conversion of the charge into payment currency (EUR) is done at the average rate of exchange set by the Polish National Bank from the day confirmation of participation has been sent.</li>
			<li>Making all payments specified by these Regulations is a condition for participation in the Marketing automation Workshops.</li>
			<li>The Organizer disclaims responsibility for the malfunction of payment systems.</li>
			<li>The Attendee is obliged to get acquainted with  the payment system regulations regarding the payment that is about to be made regarding the cost of attendance.</li>
		</ol>
		<p class="terms__heading"><strong>IV Resignation Policy</strong></p>
		<ol class="terms__list">
			<li>Attendee may file resignation from participation in the Marketing automation Workshops. The Attendee shall be eligible for a 100% refund if they cancel their attendance at least 7 days prior to the  the Marketing automation Workshops. </li>
			<li>If Attendee cancels minimum 3 days prior to the  the Marketing automation Workshops, they are eligible for a 50% refund.</li>
			<li>The statement of resignation from participation in the Marketing automation Workshops should be made in writing and sent to workshops@salesmanago.pl. Removal from the register of Attendees shall be effected as soon as a notification has been received and the confirmation from Organizer has been sent.</li>
		</ol>
		<p class="terms__heading"><strong>V Privacy Policy</strong></p>
		<ol class="terms__list">
			<li>The Organizer collects your personal information to provide its services, fulfil the transaction and enable any claims regarding the payment for products or services purchased.</li>
			<li>Upon the consent of the Attendee collected general information may be stored in  one database or more, which are directly or indirectly held by administrator - Organizer and the third parties cooperating with Organizer in the context of the Marketing automation Workshops.</li>
			<li>The administrator takes all steps reasonably necessary to protect against the unauthorized access, use, alteration, disclosure or destruction of Personal Information.</li>
			<li>Communication between the Attendee’s computer and Organizer’s software during completing the transaction is encoded with SSL (Secure Socket Layer).</li>
		</ol>
		<p class="terms__heading"><strong>VI Attendee Conduct</strong></p>
		<ol class="terms__list">
			<li>Each Attendee shall comply with all conditions set forth below or as otherwise notified to the Attendee by the Organizer.</li>
			<li>An Organizer or its designee has the right at the the Marketing automation Workshops to record, film and photograph. The Organizer reserves the right to use images taken at the the Marketing automation Workshops with your photograph and likeness in future marketing materials.The person who does not agree on such terms shall notify the Organizer by email, the latest after the first day of the Marketing automation Workshops’ ending.</li>
		</ol>
		<p class="terms__heading"><strong>VII Personal Information Change</strong></p>
		<ol class="terms__list">
			<li>The Attendee is not allowed to change information used to make a purchase, neither delete them, since they are the part of the charge account.</li>
			<li>Attendees have the right to access their personal information and edit them. Removing personal information requires a direct contact with the Organizer and the request sent to the email address: workshops@salesmanago.pl</li>
		</ol>
		<p class="terms__heading"><strong>VIII Final provisions</strong></p>
		<ol class="terms__list">
			<li>The Attendee covers commuting cost on their own.</li>
			<li>The Organizer has the right in case of unexpected and unpredictable events to cancel or postpone the the Marketing automation Workshops. In the event of a postponing or a cancellation stemming from the circumstances for which the Organizer is responsible, the Attendee's claims are limited to the amount of the ticket price paid for the Workshop.</li>
			<li>In case of issues not stated above, the Organizer applies to the provisions of the Polish Civil Code procedure.</li>
			<li>Acceptance of terms and conditions of the the Marketing automation Workshops simultaneously constitute the consent to the Privacy Policy of services provided by Benhauer.</li>
			<li>After Organizer’s notification about the changes in terms and conditions the Attendee shall forthwith become acquainted with them. In case of no acceptance of changed policies, the Attendee shall notify the Organizer about a decision via email: workshops@salesmanago.pl in a period of 7 days since the change was implemented. If the Organizer does not receive declaration about the lack of acceptance from the Attendee, it is assumed, that the Attendee has accepted the changes.</li>
			<li>These General Terms and Conditions shall enter into force on 5th of April 2017.</li>
		</ol>
	</div>
</section>

<?php get_footer(); ?>
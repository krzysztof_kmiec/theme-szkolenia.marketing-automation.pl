<?php
/**
 * Template Name: registration
 **/

if(!isset($_POST['first-name'])){
    header("Location: http://szkolenia.marketing-automation.pl");
}

get_template_part('analytics-scripts/salesmanago', 'integration');
 
$obj = array(
    'company-name' => (isset($_POST['company-name'])) ? htmlspecialchars($_POST['company-name']) : '',
    'first-name' => htmlspecialchars($_POST['first-name']),
    'last-name' => htmlspecialchars($_POST['last-name']),
    'email' => htmlspecialchars($_POST['email']),
    'phone' => htmlspecialchars($_POST['phone']),
    'tag-1' => htmlspecialchars($_POST['tag-sm-1']),
    'tag-2' => htmlspecialchars($_POST['tag-sm-2']),
    'tag-1' => htmlspecialchars($_POST['tag-sm-1']),
    'consentProcessing' => $_POST['processing_data'],
    'consentCommercial' => $_POST['electronic_communication']
);

$sm_user = new SM_API($obj);

?>
<?php get_header(); ?>

    <div class="container">
        <div class="ma-head" style="padding: 50px 0px; font-size: 24px; line-height: 28px;">
            Dziękujemy za dokonanie rejestracji.<br> <br>Jeżeli masz pytania, skontaktuj się z nami: bok@salesmanago.pl<br> <br> Nie zapomnij komputera! Do zobaczenia na szkoleniu!
        </div>
    </div>

    <div id="register" class="form__content">
        <?php get_template_part('template-parts/content', 'billing-info'); ?>
    </div>

    <section class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="banner-heading">Szkolenia dla <br> Klientów i Partnerów</h1>
                    <img class="sales-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/smlogo.png" alt="SALESmanago logo">
                </div>
            </div>
        </div>
    </section>

    <section class="organisers" id="organizatorzy">
        <div class="container">
            <div class="row">
                <div class="org-head col-md-12">
                    Organizatorzy
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="org-item">
                        <a href="https://www.salesmanago.pl/" target="blank" alt="">
                            <span class="media salesmanago"></span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="org-item">
                        <a href="http://www.benhauer.pl/" target="blank" alt="">
                            <span class="media benhauer"></span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="org-item">
                        <a href="http://edukacja.salesmanago.pl/" target="blank" alt="">
                            <span class="media education"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="platform" style="padding: 15px 0 0 0;">
        <div class="container">
            <div class="row row-centered">
                <div class="col-md-10 col-centered">
                    <div class="e-learning">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/graf.jpg" alt="certified specialist logo img-responsive">
                            </div>
                            <div class="col-md-8">
                                <h3>Platforma e-learningowa SALESmanago</h3>
                                <p>Ukończ kurs i zdobądź <a href="http://elearning.salesmanago.pl" class="free-certificate">darmowy certyfikat</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
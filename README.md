## Usage

### 1. Install nodejs, gulp, ruby-compass

sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm
sudo apt-get install ruby-compass
npm install -g gulp
npm install -g gulp-cli


### 2. Make sure nodejs with npm is installed on your machine

nodejs -v
npm -v
npm -v
ruby -v 
gem list 

### 3. Install all dependencies,  you need open terminal and write this command

npm install

### 4. Run default gulp task (will open browser window with livereload)

gulp

## Build

In order to build the production version of your project run __gulp build__ from the root of cloned repo.

## List of npm packaged used

- gulp
- browser-sync
- gulp-sass
- gulp-sourcemaps
- gulp-auroprefixer
- gulp-clean-css
- gulp-uglify
- gulp-concat
- gulp-imagemin
- gulp-changed
- gulp-html-replace
- gulp-htlmin
- del
- run-sequence

## fix problem with watches on linux
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p


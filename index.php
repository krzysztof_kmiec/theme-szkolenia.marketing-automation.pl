<?php get_header(); ?>

<div id="register" class="form__content">
    <?php get_template_part('template-parts/content', 'billing-info'); ?>
</div>

<section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="banner-heading">Szkolenia dla <br> Klientów i Partnerów</h1>
                <img class="sales-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/smlogo.png" alt="SALESmanago logo">
            </div>
        </div>
    </div>
</section>

<nav class="client-partner-nav">
    <ul>
        <li><a class="agenda-link klient-link">Klient</a></li>
        <li><a class="agenda-link partner-link">Partner</a></li>
    </ul>
</nav>

<section class="description description-klient" id="description">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="training-description">
                    <h3>Serdecznie zapraszamy na jednodniowe szkolenie wszystkich klientów SALESmanago. </h3>
                    <p class="training-description__event">Wydarzenie jest bezpłatne i odbędzie się <strong>6 sierpnia 2018 roku.</strong></p>
                    <p>Celem szkolenia jest przekazanie wiedzy o systemie SALESmanago Marketing Automation. Jeżeli dopiero zaczynasz tworzyć swoją kampanię Marketing Automation, na pewno przyda Ci się wiedza o tym, jak to zrobić.</p>
                    <p>Oczywiście, masz wszystkie narzędzia do tego, żeby się uczyć:</p>
                    <ul>
                        <li>webinary</li>
                        <li>online kurs na platformie e-learningowej SALESmanago</li>
                        <li>stronę pomocy</li>
                        <li>ebooki i wiele innego</li>
                    </ul>
                    <p>Ale podczas szkolenia pokażemy Ci, jak przejść przez pierwsze 10 kroków konfiguracji systemu: razem wprowadzimy pierwsze ustawienia na Twoim koncie - a więc nie zapomnij <strong>zabrać ze sobą komputera!</strong></p>
                    <p><strong>Zalety</strong>:</p>
                    <ul>
                        <li>nie będzie nudnych prezentacji - działamy w systemie!</li>
                        <li>wymieniamy się pomysłami - burza mózgów na temat Marketing Automation</li>
                        <li>case Study - nie musisz czytać tysiąca ebook’ów: opowiemy Ci wszystko</li>
                        <li>po szkoleniu będzie w stanie samodzielnie skonfigurować system i zacząć pracę z SALESmanago Marketing Automation</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="ma-triangle"></div>
    </div>
</section>

<section class="description description-partner" id="description">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="training-description">
                    <h3>Chcielibyśmy zaprosić Państwa na spotkanie dla partnerów i resellerów SALESmanago.</h3>
                    <p>Ten 2-dniowy event odbędzie się w Krakowie i ma na celu udzielenie wszelkich informacji na temat Marketing Automation, jakie są potrzebne w procesie sprzedażowym. Nasi eksperci omówią możliwości, funkcjonalności, studia przypadków i najbardziej skuteczne praktyki sprzedaży.</p>
                    <p>Data: <strong>20-21 września 2018</strong></p>
                    <p>Wszyscy uczestnicy są również zaproszeni na SALESmanago After Party.</p>
                </div>
            </div>
        </div>
        <div class="ma-triangle"></div>
    </div>
</section>

<section id="agenda">
    <div class="container">
        <div class="loc-head agenda">Program Szkolenia 06.08.2018</div>
    </div>
</section>

<section class="program program-klient" id="program">
    <?php get_template_part('template-parts/content', 'klient'); ?>
</section>

<section class="program program-partner" id="program">
    <?php get_template_part('template-parts/content', 'partner'); ?>
</section>

<section class="wykladowcy wykladowcy--klient" id="wykladowcy">
    <div class="container">
        <div class="row">
            <div class="wy-head">
                Wykładowcy
            </div>
        </div>
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="speaker">
                    <div class="row">
                        <div class="col-md-4 col-md-push-8">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/mc2.jpg" alt="Magdalena Calik" class="img-responsive img-circle center-block" style="filter: grayscale(100%);-webkit-filter: grayscale(100%);">
                            </div>
                        </div>
                        <div class="col-md-8 col-md-pull-4">
                            <div class="speaker-name center-mobile">Magdalena Calik</div>
                            <div class="speaker-desc">
                                <p>
                                    Studentka Dziennikarstwa i komunikacji społecznej, która szturmem weszła na rynek Marketing Automation. Początkowo edukowała kontrahentów z Partner Channel, dziś przygotowuje klientów do pracy w systemie. Prywatnie pasjonuje ją kuchnia fusion. Pomocna, żywiołowa i perfekcyjna w każdym CALU.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="wykladowcy wykladowcy--partner" id="wykladowcy">
    <div class="container">
        <div class="row">
            <div class="wy-head">
                Wykładowcy
            </div>
        </div>
        <div class="row row-centered">
            <div class="col-md-10 col-centered">
                <div class="speaker">
                    <div class="row">
                        <div class="col-md-4 col-md-push-8">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pn.jpg" alt="Paulina Nowakowska" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8 col-md-pull-4">
                            <div class="speaker-name center-mobile">Paulina Nowakowska</div>
                            <div class="speaker-desc">
                                <p>
                                    Od samego początu swojej kariery w SALESmanago Paulina edukuje ludzi na temat działania systemu. Początkowo szerzyła wiedzę wśród klientów w dziale sprzedaży, a obecnie obsługuje Partnerów w topowym teamie w supporcie. Kiedy nie opowiada o najnowszych trendach w automatyzacji fascynuje się życiem, tradycją i kulturą narodów Dalekiego Wschodu, zwłaszcza Korei ( Północnej i Południowej ).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="speaker">
                    <div class="row ">
                        <div class="col-md-4">
                            <div class="speaker-pic">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/as.jpg" alt="Aleksander Skałka" class="img-responsive img-circle center-block">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="speaker-name center-mobile">Aleksander Skałka</div>
                            <div class="speaker-desc">
                                <p>Dyrektor Strategii Marketing Automation. Absolwent Uniwersytetu Jagiellońskiego. Odpowiada za rozwój produktu i obsługę projektów SALESmanago oraz budowanie wiedzy na temat możliwości systemu. Posiada bogate doświadczenie przy wdrażaniu zaawansowanych procesów automatyzacji marketingu i sprzedaży. Prywatnie wokalista heavy metalowy.
                                </p>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>



<section class="organisers" id="organizatorzy">
    <div class="container">
        <div class="row">
            <div class="org-head col-md-12">
                Organizatorzy
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="https://www.salesmanago.pl/" target="blank" alt="">
                        <span class="media salesmanago"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://www.benhauer.pl/" target="blank" alt="">
                        <span class="media benhauer"></span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="org-item">
                    <a href="http://edukacja.salesmanago.pl/" target="blank" alt="">
                        <span class="media education"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="platform">
    <div class="container">
        <div class="row row-centered">
            <div class="col-md-12 col-centered">
                <div class="e-learning" style="border: 1px solid #eee; padding: 15px;">
                    <div class="row">
                        <div class="col-md-2 text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/graf.jpg" alt="certified specialist logo img-responsive">
                        </div>
                        <div class="col-md-8 elearning-platform">
                            <h3 class="elearning-platform__header" style="font-size: 2em;">Zostań certyfikowanym Ekspertem Marketing Automation</h3>
                            <p class="elearning-platform__subheader" style="font-size: 1.25em;">Zarejestruj się na naszej <span class="elearning-platform--green">platformie E-learningowej</span>, aby uzyskać wyjątkową, bardzo cenną wiedzę, która zapewni Ci sukces na Twojej ścieżce kariery zawodowej</p>
                        </div>
                        <div class="col-md-2 elearning-button">
                            <a href="http://elearning.salesmanago.pl/" target="_blank" class="btn btn-u-lg elearning-register__button" style="background-color: #87ac29; color: #fff; padding: 8px 30px;"s>Rejestracja</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

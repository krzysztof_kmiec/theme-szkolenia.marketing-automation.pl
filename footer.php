<section class="localization" id="location">
    <div class="container localization__container localization__container--warsaw">
        <div class="row">
            <div class="loc-head">
                Lokalizacja
            </div>
        </div>
        <div class="row row-centered">
            <div class="col-md-6 col-centered">
                <div class="text-center">
                    <h3>BENHAUER</h3>
                    <p>ul. Grzegórzecka 21</p>
                    <p>Kraków</p>
                </div>
            </div>
        </div>
        <!-- MAPKA GOOGLE -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2561.489243862128!2d19.950631451041005!3d50.05839857932225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47165b0eb3bd2069%3A0x1cca5895788dbc1e!2sBenhauer+Sp.+z+o.o.!5e0!3m2!1spl!2spl!4v1496846250856" width="600" height="450" frameborder="0" style="border:0; width:100%;" allowfullscreen></iframe>
        <!-- KONIEC MAPKI -->
    </div>
</section>

<div class="foot-social">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="salesmanago-big"></span>
            </div>
            <div class="col-md-8">
                <div class="social">
                    <div class="social_icons_down clearfix">
                        <span class="follow-us">Follow us: </span>
                        <a target="_blank" href="https://www.facebook.com/SALESmanago">
                        <span class="facebook"></span>
                        </a>
                        <a target="_blank" href="https://www.linkedin.com/company/5134015/">
                        <span class="linkedin"></span>
                        </a>
                        <a target="_blank" href="https://www.youtube.com/channel/UC0-su31DYGaQ3cLmbUnCOPA">
                        <span class="youtube"></span>
                        </a>
                        <a target="_blank" href="https://twitter.com/SALESmanago">
                        <span class="twitter"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-buttons">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="https://www.salesmanago.com" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Marketing automation system</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://blog.salesmanago.com" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Marketing automation blog</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://elearning.salesmanago.pl/" target="_blank">
                    <div class="foot-button">
                        <div class="foot-button-text">Marketing Automation Course</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul>
                    <li>©
                        <?php echo date('Y'); ?> SALESmanago</li>
                    <li>All rights reserved</li>
                    <li>this website uses cookies</li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script>
    (function() {

        $('.open-register').on('click', function() {
            $('#register').toggleClass('form__content--active');
            /*
            if($('#billing-info').hasClass("hide")){
                setTimeout(function(){
                    $('#billing-info').removeClass('hide');
                    $('#recurly').addClass('hide');
                    $('#paypal').addClass('hide');
                }, 1500)
            }*/
        });

        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

    })();

</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>

<!--

<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/iframeResizer.min.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/jssor.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/jssor.slider.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>

-->
<?php require_once('analytics-scripts/salesmanago.php'); ?>
<?php require_once('analytics-scripts/googleanalytics.php'); ?>

<script>
    (function() {

        var btnList = $(".item-list");
        btnList.delegate("input", "focusout", function() {
            var id = $(this).attr('id').toString().slice(0, -3);
            var text = $(this).val();
            $('#' + id).val(text);
        });

        btnList.delegate("select", "focusout", function() {
            var id = $(this).attr('id').toString().slice(0, -3);
            var text = $(this).val();
            $('#' + id).val(text);
        });

        var btn = document.getElementById('payment-chosen');

        btn.addEventListener('click', function() {

            var paymentMethod = $('.payment__method input[name=payment]:checked').val();
            $('#form').attr('action', paymentMethod);

            /*
            if(paymentMethod === 'card'){
                $('#billing-info').addClass('hide');
                $('#recurly').removeClass('hide');
                $(document).scrollTo('#recurly');
                console.log('card');
            }else if(paymentMethod === 'paypal'){
                $('#billing-info').addClass('hide');
                $('#paypal').removeClass('hide');
                $(document).scrollTo('#paypal');
                console.log('paypal');
            }
            */

            //            console.log('test');
            //           $('.payment__method input').on('change', function() {
            //               console.log($('input[name=payment]:checked').val());
            //           });

        });

        var dateforClient = '06.08.2018';
        var dateforPartner = '20-21.09.2018';

        $('.program-partner, .description-partner, .wykladowcy--partner').hide();
        $('.klient-link').addClass('active');
        $('.agenda').text('Program Szkolenia ' + dateforClient);

        $('.agenda-link').on('click', function() {
            $('.agenda-link').removeClass('active');
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });

        $('.klient-link').on('click', function() {
            $('.program, .description, .wykladowcy').hide();
            $('.agenda').text('Program Szkolenia ' + dateforClient);
            $('.program-klient, .description-klient, .wykladowcy--klient').fadeIn();

        });

        $('.partner-link').on('click', function() {
            $('.program, .description, .wykladowcy').hide();
            $('.agenda').text('Program Szkolenia ' + dateforPartner);
            $('.program-partner, .description-partner, .wykladowcy--partner').fadeIn();
        });

        //$("#country option[value='AF']").attr("selected", true);

        //questions.children().eq($(this).index() + 1).slideToggle('slow');
        /*
         if($(this).hasClass('showAnswer')){
         $(this).removeClass('showAnswer');
         console.log('hide');
         questions.children().eq($(this).index() + 1).hide(300);
         }else{
         console.log('show');
         $( this ).addClass('showAnswer');
         questions.children().eq($(this).index() + 1).slideDown(700);

         }
         */

        /*
        console.log('Test');
        itemList = document.querySelector(".item-list");

        // Event delegation
        itemList.addEventListener('click', function (ev) {
            if (ev.target.nodeName === 'LI') {
                alert('item new');
            }
        }, false);
        */
    })();

</script>
<script>
    $(function() {
        var popup = $('<div class="popup">Wyrażam zgodę na badanie przez Benhauer moich preferencji i zachowań, aby umożliwić dostarczenie spersonalizowanych treści oraz polepszyć funkcjonowanie niniejszej strony. Zgoda może być cofnięta w każdym czasie poprzez wysłanie wiadomości e-mail lub zmianę ustawień przeglądarki internetowej.<span class="popup__close">+</span></div>');

        if (localStorage.getItem('popState') !== 'shown') {
            $('body').append(popup);
            $('.popup__close').on('click', function() {
                $('.popup').hide();
            });
            localStorage.setItem('popState', 'shown');
        }
    });
</script>
</body>

</html>

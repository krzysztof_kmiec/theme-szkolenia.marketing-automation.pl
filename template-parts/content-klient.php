<div class="container">
	<div class="row row-centered">
		<div class="col-md-10 col-centered">
			<div class="schedule-container">
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    11:00 - 11:30
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                        Marketing Automation - wprowadzenie
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    11:30 - 12:00 
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                         Integracja strony i konfiguracja konta  
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> monitorowanie kontaktów: jak to działa? </li>
			                            <li><i class="fa fa-play-circle"></i> integracja z platformą sklepową/ stroną www</li>
			                            <li><i class="fa fa-play-circle"></i> ustawienia konta wysyłkowego oraz kont użytkownika</li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    12:00 - 12:05
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Przerwa kawowa
			                    </div>

			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    12:05 - 13:00
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                        Baza kontaktów i pozyskiwanie leadów
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> CRM: jak zaimportować kontakty oraz poprawnie odczytać informacje na karcie kontaktu </li>
			                            <li><i class="fa fa-play-circle"></i> Sposoby pozyskania kontaktów</li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    13:00 - 14:00
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Lunch
			                    </div>

			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                 14:00 - 15:30
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                        Komunikacja - kampanie masowe i automatyczne 
			                    </div>
			                    <div class="schedule-desc">
			                        <ul>
			                            <li><i class="fa fa-play-circle"></i> E-mail Marketing: tworzenie wiadomości oraz sposoby wysyłki (+ treści dynamiczne) </li>
			                            <li><i class="fa fa-play-circle"></i> Procesy automatyzacji i segmentacja kontaktów</li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    15:30 - 15:35
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title">
			                       Przerwa kawowa
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			    <div class="schedule-row">
			        <div class="row">
			            <div class="col-md-2">
			                <div class="schedule-time">
			                    15:35 - 16:30
			                </div>
			            </div>
			            <div class="col-md-1">
			                <div class="schedule-tick schedule-tick-disabled visible-lg">

			                </div>
			            </div>
			            <div class="col-md-9">
			                <div class="schedule-content-box">
			                    <div class="schedule-title center-mobile">
			                        Case Study / warsztaty 
			                    </div>

			                </div>
			            </div>
			        </div>
			    </div>
			    <!-- wpis -->
			</div>
		</div>
	</div>
</div>

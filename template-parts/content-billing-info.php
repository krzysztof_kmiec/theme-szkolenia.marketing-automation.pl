<style>
            .processing_accept {
            width: auto;
            height: auto;
            display: inline-block;
            margin: 0 !important;
        }
        .processing_text {
            font-weight: normal;
            margin-left: 5px;
            color: #333 !important;
            text-align: left !important;
            font-size: 11px;
        }
        .processing_text a {
            color: #85a92d;
        }
        .accept__container label{
            margin-bottom: 7px;
                        text-transform: unset !important;
        }
</style>
   <div id="billing-info" class="recurly__form">
    <div class="container text-center">
        <h3>Rejestracja na szkolenia</h3>
        <p class="form__text">Poniżej znajduje się formularz rejestracji. Na kilka dni przed wydarzeniem dostaniesz od nas e-mail ze wszystkimi niezbędnymi informacjami.</p>
        <p class="form__text">Uwaga: ilość miejsc jest ograniczona do 15 osób.</p>

        <div class="row" style="padding-top: 20px">

            <form id="form" method="post" action="rejestracja" novalidate>

                <!--
                <div class="col-xs-12">
                    <h3>Dane personalne</h3>
                </div>
                -->

                <div class="col-md-6 col-sm-12">

                    <div>
                        <label for="company_name">Firma</label>
                        <input type="text" name="company-name">
                    </div>

                    <div>
                        <label for="first_name">Imię<span>*</span></label>
                        <input type="text" name="first-name" data-error-message="imię" required>
                    </div>

                    <div>
                        <label for="last_name">Nazwisko<span>*</span></label>
                        <input type="text" name="last-name" data-error-message="nazwisko" required>
                    </div>

                </div>
                <div class="col-md-6 col-sm-12">

                    <div>
                        <label for="email">Adres e-mail<span>*</span></label>
                        <input type="email" name="email" id="email" data-error-message="email" required>
                    </div>

                    <div>
                        <label for="phone">Telefon<span>*</span></label>
                        <input type="text" name="phone" data-error-message="telefon" required>
                    </div>

                    <div>
                        <label for="trainings">Typ szkolenia<span>*</span></label>
                        <select class="select__country" id="selectTraining" name="trainings" data-error-message="typ szkolenia" required>
                            <option value=""></option>
                            <option>klient</option>
                            <option>partner</option>
                        </select>
                        <input type="hidden" id="tag-sm-1" name="tag-sm-1" value="">
                        <input type="hidden" id="tag-sm-2" name="tag-sm-2" value="">
                    </div>

                </div>
                <div class="col-xs-12">
                    <div class="col-md-6 col-md-offset-3 col-sm-12">
                        <button type="submit" id="payment-chosen" style="margin-top: 20px;width: 160px;">Wyślij</button>

                        <!--
                        <div class="form-group" style="text-align: left;margin-left: 30px;">
                            <div class="checkbox">
                                <input type="checkbox" data-error-message="accetpt regulations" required checked>Akceptuję <a href="<?php echo get_page_link(15); ?>" target="_blank">regulamin</a><span>*</span>
                            </div>
                        </div>
                        -->

                        <p style="text-align: center; padding-top: 15px;">*Wymagane pola</p>
                                            <div style="text-align: left;margin-left: 10px;" class="accept__container">
    <label><input type="checkbox" name="processing_data" id="processing_data" class="processing_accept" required="required" required data-error-message="pozwolenie na przetwarzanie danych" value="true"><span class="processing_text articleParagraph">Zgadzam się na przetwarzanie moich danych osobowych w marketingu spółki Benhauer. </span><span class="data__required">*</span></label>

<label><input type="checkbox" name="electronic_communication" id="electronic_communication" class="processing_accept" required="required" required data-error-message="pozwolenie na otrzymywanie informacji handlowych" value="true"><span class="processing_text articleParagraph">Zgadzam się na otrzymywanie informacji handlowych drogą elektroniczną o produktach Benhauer i jego partnerów. </span><span class="data__required"> *</span></label>

<p class="articleParagraph processing_text" style="margin-left: 0;">Poprzez kliknięcie w ten przycisk potwierdzasz zapoznanie się z informacjami nt <a href="https://www.salesmanago.pl/info/obowiazek_informacyjny.htm">przetwarzania danych osobowych</a></p>
</div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="alert"></div>
                </div>

            </form>

        </div>

    </div>
</div>
<script>
    (function() {

        function addSmTag() {
            var select = document.getElementById('selectTraining');
            var selectedOption = select.options[select.selectedIndex].value;

            var tag1 = document.getElementById('tag-sm-1');
            var tag2 = document.getElementById('tag-sm-2');

            if (selectedOption === 'klient') {
                tag1.value = 'szkolenia_klientow_pl';
                tag2.value = 'szkolenia_klientow_pl_08.2018';
            } else if (selectedOption === 'partner') {
                tag1.value = 'szkolenia_partnerskie_pl';
                tag2.value = 'szkolenia_partnerskie_pl_09.2018';
            } else {
                tag1.value = '';
                tag2.value = '';
            }
        }

        document.getElementById('selectTraining').addEventListener('change', function() {
            addSmTag();
        });

        document.getElementById('form').addEventListener('submit', function(e) {
            e.preventDefault();

            var data = {
                email: document.getElementById('email').value
            }

            $.ajax({
                type: 'POST',
                url: '<?php echo get_template_directory_uri(); ?>/sendEmail.php',
                data: data
            });

        });

        function Validator(form) {
            this.form = form;
            this.fields = this.form.querySelectorAll("[required]");
            this.errors = [];
            this.errorsList = this.form.querySelector(".alert");

            if (!this.fields.length) return;

            this.form.onsubmit = function(e) {
                e.preventDefault();

                var formValid = this.validate();

                if (formValid) {
                    addSmTag();
                    this.form.submit();
                } else {
                    return false;
                }
            }.bind(this);
        }

        Validator.prototype.validate = function() {
            this.clearErrors();

            for (var i = 0; i < this.fields.length; i++) {
                this.validateField(this.fields[i]);
            }

            if (!this.errors.length) {
                return true;
            } else {
                this.showErrors();
                return false;
            }
        };

        Validator.prototype.validateField = function(field) {
            var fieldValid = field.validity.valid;

            if (fieldValid) {
                this.markAsValid(field);
            } else {
                this.errors.push(field.dataset.errorMessage);
                this.markAsInvalid(field);
            }
        };

        Validator.prototype.markAsValid = function(field) {
            field.classList.remove("invalid");
            field.classList.add("valid");
        };

        Validator.prototype.markAsInvalid = function(field) {
            field.classList.remove("valid");
            field.classList.add("invalid");
        };

        Validator.prototype.showErrors = function() {
            var msg = 'Poniższe pola są nieprawidłowe: ';

            for (var i = 0; i < this.errors.length; i++) {
                msg += this.errors[i];
                msg += ', ';
            }
            msg = msg.slice(0, -2);

            this.errorsList.innerHTML = msg;
            this.errorsList.style.display = "block";
        };

        Validator.prototype.clearErrors = function() {
            this.errors.length = 0;
        };

        var validator1 = new Validator(document.querySelector("#form"));

    })();

</script>

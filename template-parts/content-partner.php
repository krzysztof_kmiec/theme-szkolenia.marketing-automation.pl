<div class="container">
    <div class="row row-centered">
        <div class="col-centered col-md-10">

            <div class="post-tabs-container">
                <div class="post-tab-buttons">
                    <div class="post-tab-button post-tab-button-pierwszy active" data-tab="pierwszy">Dzień pierwszy</div>
                    <div class="post-tab-button post-tab-button-drugi" data-tab="drugi">Dzień drugi</div>
                </div>
                <div style="clear:both"></div>
                <div class="post-tab-content post-tab-content-pierwszy">
                    <div class="schedule-container">
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        9:00 - 9:30
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Kawa i networking
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        9:30 - 10:45
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Wstęp do Marketing Automation: przegląd, możliwości i główne funkcje
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Paulina Nowakowska, Marketing Automation Specialist</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        10:45 - 12:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick-disabled visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title center-mobile">
                                            SALESmanago Marketing Automation: konfiguracja systemu
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <!-- <li><i class="fa fa-play-circle"></i> Aleksander Skałka, Marketing Automation Strategy Director, SALESmanago Marketing Automation</li> -->
                                                <li><i class="fa fa-play-circle"></i> Paulina Nowakowska, Marketing Automation Specialist</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        12:00 - 13:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Lunch
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        13:00 - 14:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick-disabled visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title center-mobile">
                                            Wykorzystanie Marketing Automation do zwiększenia sprzedaży w B2B
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Paulina Nowakowska, Marketing Automation Specialist</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        14:00 - 14:30
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Przerwa kawowa
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        14.30 - 15.30
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Marketing Automation w e-commerce - zwiększenie konwersji dzięki dynamicznym treściom
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Paulina Nowakowska, Marketing Automation Specialist</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        15:30 - 16:30
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick-disabled visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title center-mobile">
                                            Zaawansowane możliwości Marketing Automation: moduł RFM, panele analityczne i inne funkcjonalności Premium
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Paulina Nowakowska, Marketing Automation Specialist</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        20:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            SALESmanago After Party
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-tab-content post-tab-content-drugi">
                    <div class="schedule-container">
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        10:00 - 11:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Benhauer / SALESmanago jako lider Marketing Automation w Europie
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Grzegorz Błażewicz, CEO, SALESmanago Marketing Automation </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        11:00 - 12:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick-disabled  visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title center-mobile">
                                            Efektywny proces sprzedażowy w kanale partnerskim SALESmanago
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Katarzyna Dudzic, Head of Polish Partnership Channel, SALESmanago Marketing Automation </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        12:00 - 13:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Lunch
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        13.00 - 14:00
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick-disabled visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title center-mobile">
                                            Jak skutecznie sprzedawać SALESmanago w B2B, B2C i e-commerce. Najlepsze praktyki i case study
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Renata Gerlach, Director of Poland Sales Department </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wpis -->
                        <div class="schedule-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="schedule-time">
                                        14:00 - 14:45
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="schedule-tick schedule-tick visible-lg">

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="schedule-content-box">
                                        <div class="schedule-title">
                                            Wstęp do Mobile Marketing Automation: przegląd, możliwości i główne funkcje platformy APPmanago
                                        </div>
                                        <div class="schedule-desc">
                                            <ul>
                                                <li><i class="fa fa-play-circle"></i> Yuri Khachatryan, Business Development Manager, Team Leader, APPmanago </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="ma-triangle"></div>
